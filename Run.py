import os
from datetime import datetime

import openpyxl
import pandas as pd

SET_VN_CHAR = set("""ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻ
						ẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđẠẢÃÀÁ
						ÂẬẦẤẨẪĂẮẰẶẲẴÓÒỌÕỎÔỘỔỖỒỐƠỜỚỢỞỠÉÈẺẸẼÊ
						ẾỀỆỂỄÚÙỤỦŨƯỰỮỬỪỨÍÌỊỈĨÝỲỶỴỸĐ""")

def convert_excel_time(excel_time):
	'''
	converts excel float format to pandas datetime object
	round to '1min' with 
	.dt.round('1min') to correct floating point conversion innaccuracy
	'''
	return pd.to_datetime('1899-12-30') + pd.to_timedelta(excel_time,'D')

def validate(date_text):
	try:
		datetime.strptime(date_text, '%Y-%m-%d %H:%M:%S')
		return True
	except ValueError:
		return False


class HS:
	def __init__(self, stt='', name='', nickname='', ma_crm='', ma_lms='',
				ma_effect='', ma_ae='', ma_ec='', ma_cm='', kyhoc='', sanpham='',
				chuongtrinh='', goiphi='', loaihocsinh='', loai_goi_phi='', giagoc='',
				tonggiamtru='', ngaybatdauhoc='', baoluu='', ngayhoccuoi='', pending='',
				taiphi='', status='', why_fail_list=None, tienphaidong='', tongsobuoihoc='',
				giamtru_ae='', sobuoidahoc='', giamtrutichdiem='', chietkhaugiagoc='', voucher=''):
		self.stt = stt
		self.name = name
		self.nickname = nickname
		self.ma_crm = ma_crm
		self.ma_lms = ma_lms
		self.ma_effect = ma_effect
		self.ma_ae = ma_ae
		self.ma_ec = ma_ec
		self.ma_cm = ma_cm
		self.kyhoc = kyhoc
		self.sanpham = sanpham
		self.chuongtrinh = chuongtrinh
		self.goiphi = goiphi
		self.loai_goi_phi = loai_goi_phi
		self.giagoc = giagoc
		self.tonggiamtru = tonggiamtru
		self.ngaybatdauhoc = ngaybatdauhoc
		self.pending = pending
		self.taiphi = taiphi
		self.status = status
		if why_fail_list is None:
			why_fail_list = []
		self.why_fail_list = why_fail_list
		self.giamtru_ae = giamtru_ae
		self.tienphaidong = tienphaidong
		self.loaihocsinh = loaihocsinh
		self.ngayhoccuoi = ngayhoccuoi
		self.tongsobuoihoc = tongsobuoihoc
		self.sobuoidahoc = sobuoidahoc
		self.giamtrutichdiem = giamtrutichdiem
		self.chietkhaugiagoc = chietkhaugiagoc
		self.voucher = voucher
		self.is_taiphi = 0

	def __str__(self):
		return '{} - {}\n'.format(self.stt, self.name)

	def why_fail(self, comment):
		self.why_fail_list.append('* '+comment+'\n')
		self.status = False

	def check_name_hs(self):
		if not self.name:
			self.why_fail('Thiếu mã tên học sinh')
		elif set(self.name) & SET_VN_CHAR:
			self.why_fail('Tên hs phải nhập không dấu')
		return

	def check_ma_crm(self):
		if not self.ma_crm or str(self.ma_crm).strip() == '0':
			self.why_fail('Bổ sung mã CRM')
		return

	def check_ma_lms(self):
		if not self.ma_lms or not str(self.ma_lms).isdigit():
			self.why_fail('Bổ sung mã LMS')
		return

	def check_ma_effect(self):
		if not self.ma_effect or str(self.ma_effect).strip() == '0':
			self.why_fail('Bổ sung mã Effect')
		return

	def check_ma_ec(self):
		if not self.ma_ec or (set(str(self.ma_ec)) & SET_VN_CHAR):
			self.why_fail('Bổ sung mã EC')
		return

	def check_ma_cm(self):
		if not self.ma_cm or (set(str(self.ma_cm)) & SET_VN_CHAR):
			self.why_fail('Bổ sung mã CM')
		return

	def check_loaihocsinh(self):
		if not self.loaihocsinh:
			self.why_fail('Bổ sung loại học sinh')
		return

	def check_loaisanpham(self):
		if not self.sanpham:
			self.why_fail('Bổ sung thông tin loại sản phẩm')
		return

	def check_chuongtrinh(self):
		if not self.chuongtrinh:
			self.why_fail('Bổ sung thông tin chương trình')
		return

	def check_goiphi(self):
		if not self.goiphi:
			self.why_fail('Bổ sung thông tin gói phí')
		return

	def check_loaigoiphi(self):
		if not self.loai_goi_phi:
			self.why_fail('Bổ sung thông tin loại gói phí')
		return

	def check_giagoc(self):
		if not self.giagoc or not str(self.giagoc).isdigit():
			self.why_fail('Bổ sung thông tin giá gốc')
		return
			
	def check_logic_bao_luu(self, list_ma_lms_baoluu=None):
		if self.ma_lms:
			if self.baoluu and str(self.baoluu).isdigit():
				self.baoluu = int(self.baoluu)
				if (self.baoluu > 0) and (str(self.ma_lms).strip() in list_ma_lms_baoluu):
					return
				self.why_fail('Thiếu nội dung trong sheet Bảo lưu')
			elif self.baoluu and str(self.baoluu).lower() =='x':
				if str(self.ma_lms).strip() in list_ma_lms_baoluu:
					return
				self.why_fail('Đánh dấu x ở cột Bảo lưu nhưng không thấy nhập bên sheet Bảo lưu')
		return

	def check_logic_pending(self, list_ma_lms_pending=None):
		if self.ma_lms and self.pending and str(self.pending).lower()=='x':
			if str(self.ma_lms).strip() in list_ma_lms_pending:
				return
			self.why_fail('Đánh dấu x ở cột Pending nhưng không thấy nhập bên sheet Pending')

	def check_logic_tai_phi(self, list_ma_lms_taiphi=None):
		if self.ma_lms:
			if self.taiphi and str(self.taiphi).lower()=='x':
				if str(self.ma_lms).strip() in list_ma_lms_taiphi:
					self.is_taiphi = 1
					return
				self.why_fail('Đánh dấu x ở cột Tái phí nhưng không thấy nhập bên sheet Tái phí')
		return

	def check_logic_tong_giam_tru(self):
		if self.tonggiamtru and not str(self.tonggiamtru).isdigit():
			self.why_fail('Tổng giảm trừ phải nhập số')

		if self.loai_goi_phi:
			if "Gói đăng ký mới (0)".lower() in self.loai_goi_phi.lower()\
			or "Gói tái phí (1)".lower() in self.loai_goi_phi.lower():

				if not self.giamtru_ae:
					self.giamtru_ae = 0

				if not self.giamtrutichdiem:
					self.giamtrutichdiem = 0

				if not self.chietkhaugiagoc:
					self.chietkhaugiagoc = 0

				if not self.voucher:
					self.voucher = 0

				if not self.giagoc:
					self.giagoc = 0

				if not self.tienphaidong:
					self.tienphaidong = 0

				if not self.tonggiamtru:
					self.why_fail('Bổ sung cột tổng giảm trừ')

				elif self.goiphi and ('cũ' not in self.goiphi.lower()):
					if int(self.tonggiamtru) - int(self.giamtru_ae)\
						- int(self.giamtrutichdiem) - int(self.chietkhaugiagoc) - int(self.voucher):
						self.why_fail("check lại vì sao Cột R lỗi không = Tổng cột(S+T+U+V)")
				
				elif self.goiphi and ('cũ' in str(self.goiphi).lower()):
					if int(self.giagoc) - int(self.tienphaidong) - int(self.tonggiamtru):
						self.why_fail("check lại vì sao Cột W +R lỗi không = Q")
		return

	def check_ma_ae(self):
		if self.giamtru_ae:
			if not str(self.giamtru_ae).isdigit():
				self.why_fail('Giảm trừ anh em phải là số')
			if not self.ma_ae:
				self.why_fail('Thiếu mã anh em')
		return

	def check_giamtrutichdiem(self):
		if self.giamtrutichdiem and not str(self.giamtrutichdiem).isdigit():
			self.why_fail('Giảm trừ tích điểm phải nhập số')
		return

	def check_sotienchietkhaugiagoc(self):
		if self.chietkhaugiagoc and not str(self.chietkhaugiagoc).isdigit():
			self.why_fail('Số tiền chiết khấu phải nhập số')
		return

	def check_voucher(self):
		if self.voucher and not str(self.voucher).isdigit():
			self.why_fail('Voucher/khác phải nhập số')
		return

	def check_sotienphaidong(self):
		if self.tienphaidong:
			if not str(self.tienphaidong).isdigit():
				self.why_fail('số tiền phải đóng phải nhập số')
			elif str(self.tienphaidong).strip() == '0' and not (int(self.giagoc) == int(self.tonggiamtru)):
				self.why_fail('giá gốc = 0 nhưng tiền phải đóng # tổng giảm trừ')
			return
		self.why_fail('Bổ sung thông tin số tiền phải đóng')
		return

	def check_ngaybatdauhoc(self):
		if self.ngaybatdauhoc:
			if not validate(str(self.ngaybatdauhoc)):
				self.why_fail('Định dạng ngày bắt đầu học đúng phải theo yyyy-mm-dd')
			return
		self.why_fail('Bổ sung thông tin ngày bắt đầu học')
		return

	def check_ngayhoccuoi(self):
		if self.ngayhoccuoi:
			if not validate(str(self.ngayhoccuoi)):
				self.why_fail('Định dạng ngày học cuối phải đúng phải theo yyyy-mm-dd')
			return
			if self.ngayhoccuoi <= self.ngaybatdauhoc:
				self.why_fail('Check lại vì sao ngày kết thúc <= ngày bắt đầu học')
			return
		self.why_fail('Bổ sung thông tin ngày học cuối')
		return		

	def check_tongsobuoihoc(self):
		if self.tongsobuoihoc:
			if not str(self.tongsobuoihoc).isdigit():
				self.why_fail('Tổng số buổi học phải là số')
				return
			return
		
		self.why_fail('Bổ sung thông tin tổng số buổi học')
		return

	def check_sobuoidahoc(self):
		if not self.sobuoidahoc:
			self.why_fail('Bổ sung thông tin số buổi học tính đến 28/06/2018')
			return
		elif not str(self.sobuoidahoc).isdigit():
			self.why_fail('số buổi đã học phải là số')
		elif self.tongsobuoihoc:
			if int(self.sobuoidahoc) > int(self.tongsobuoihoc):
				self.why_fail('Check lại tổng số buổi học tính đến 28/06/2018 lại > Tổng số buổi học')
		return


EXCEL_PATH = './import.xlsx'
wb = openpyxl.load_workbook(EXCEL_PATH, data_only=True)	# chỉ load value thay vì công thức
try:
	DSHS_sheet = wb.get_sheet_by_name('DS HS')
except:
	DSHS_sheet = wb.get_sheet_by_name('DSHS')
maxRow_DSHS = DSHS_sheet.max_row

try:
	TAIPHI_sheet = wb.get_sheet_by_name('Tai phi')
except:
	TAIPHI_sheet = wb.get_sheet_by_name('Tái phí')
maxRow_TAIPHI = TAIPHI_sheet.max_row

PENDING_sheet = wb.get_sheet_by_name('Pending')
maxRow_PENDING = PENDING_sheet.max_row

try:
	BAOLUU_sheet = wb.get_sheet_by_name('Bao luu')
except:
	BAOLUU_sheet = wb.get_sheet_by_name('Bảo lưu')
maxRow_BAOLUU = BAOLUU_sheet.max_row

list_hs = []
for row in range(2, maxRow_DSHS+1):
	hs = HS()
	hs.stt =			DSHS_sheet['A'+str(row)].value
	hs.name = 			DSHS_sheet['B'+str(row)].value
	if hs.name is None:
		continue
	hs.nickname = 		DSHS_sheet['C'+str(row)].value
	hs.ma_crm = 		DSHS_sheet['D'+str(row)].value
	hs.ma_lms = 		DSHS_sheet['E'+str(row)].value
	hs.ma_ae =			DSHS_sheet['F'+str(row)].value
	hs.ma_effect = 		DSHS_sheet['G'+str(row)].value
	hs.ma_ec =			DSHS_sheet['H'+str(row)].value
	hs.ma_cm =			DSHS_sheet['I'+str(row)].value
	hs.loaihocsinh = 	DSHS_sheet['J'+str(row)].value
	hs.kyhoc =			DSHS_sheet['L'+str(row)].value
	hs.sanpham = 		DSHS_sheet['M'+str(row)].value
	hs.chuongtrinh = 	DSHS_sheet['N'+str(row)].value
	hs.goiphi = 		DSHS_sheet['O'+str(row)].value
	hs.loai_goi_phi = 	DSHS_sheet['P'+str(row)].value

	hs.giagoc = 		DSHS_sheet['Q'+str(row)].value
	if hs.giagoc:
		try:
			hs.giagoc = int(hs.giagoc)
		except:
			pass

	hs.giamtrutichdiem= DSHS_sheet['T'+str(row)].value
	if hs.giamtrutichdiem:
		try:
			hs.giamtrutichdiem = int(hs.giamtrutichdiem)
		except:
			pass

	hs.tonggiamtru = 	DSHS_sheet['R'+str(row)].value
	if hs.tonggiamtru:
		try:
			hs.tonggiamtru = int(hs.tonggiamtru)
		except:
			pass

	hs.giamtru_ae = 	DSHS_sheet['S'+str(row)].value
	if hs.giamtru_ae:
		try:
			hs.giamtru_ae = int(hs.giamtru_ae)
		except:
			pass

	hs.chietkhaugiagoc= DSHS_sheet['U'+str(row)].value
	if hs.chietkhaugiagoc:
		try:
			hs.chietkhaugiagoc = int(hs.chietkhaugiagoc)
		except:
			pass

	hs.voucher =		DSHS_sheet['V'+str(row)].value
	if hs.voucher:
		try:
			hs.voucher = int(hs.voucher)
		except:
			pass

	hs.tienphaidong = 	DSHS_sheet['W'+str(row)].value
	if hs.tienphaidong:
		try:
			hs.tienphaidong = int(hs.tienphaidong)
		except:
			pass

	hs.ngaybatdauhoc = 	DSHS_sheet['Y'+str(row)].value
	hs.ngayhoccuoi = 	DSHS_sheet['Z'+str(row)].value
	hs.tongsobuoihoc =  DSHS_sheet['AA'+str(row)].value
	hs.sobuoidahoc = 	DSHS_sheet['AB'+str(row)].value
	hs.baoluu = 		DSHS_sheet['AC'+str(row)].value
	hs.pending = 		DSHS_sheet['AD'+str(row)].value
	hs.taiphi = 		DSHS_sheet['AE'+str(row)].value
	hs.status = 		DSHS_sheet['AG'+str(row)].value
	hs.row = row
	list_hs.append(hs)

list_ma_lms_baoluu = []
hs_baoluu = []
for row in range(2, maxRow_BAOLUU+1):

	lms_value = BAOLUU_sheet['D'+str(row)].value
	if lms_value and isinstance(lms_value, str):
		list_ma_lms_baoluu.append(lms_value.strip())
	else:
		list_ma_lms_baoluu.append(lms_value)


list_ma_lms_pending = []
for row in range(2, maxRow_PENDING+1):
	lms_value = PENDING_sheet['D'+str(row)].value
	if lms_value and isinstance(lms_value, str):
		list_ma_lms_pending.append(lms_value.strip())
	else:
		list_ma_lms_pending.append(lms_value)

list_ma_lms_taiphi = []
for row in range(2, maxRow_TAIPHI+1):
	lms_value = TAIPHI_sheet['D'+str(row)].value
	if lms_value and isinstance(lms_value, str):
		list_ma_lms_taiphi.append(lms_value.strip())
	else:
		list_ma_lms_taiphi.append(lms_value)

def main():
	for i in list_hs:
		try:
			i.check_name_hs()
			i.check_ma_crm()
			i.check_ma_lms()
			i.check_ma_effect()
			i.check_ma_ec()
			i.check_ma_cm()
			i.check_loaihocsinh()
			i.check_loaisanpham()
			i.check_chuongtrinh()
			i.check_goiphi()
			i.check_loaigoiphi()
			i.check_giagoc()
			i.check_logic_bao_luu(list_ma_lms_baoluu)
			i.check_logic_pending(list_ma_lms_pending)
			i.check_logic_tai_phi(list_ma_lms_taiphi)
			i.check_logic_tong_giam_tru()
			i.check_ma_ae()
			i.check_giamtrutichdiem()
			i.check_sotienchietkhaugiagoc()
			i.check_voucher()
			i.check_sotienphaidong()
			i.check_ngaybatdauhoc()
			i.check_ngayhoccuoi()
			i.check_tongsobuoihoc()
			i.check_sobuoidahoc()

			if not i.status and i.why_fail_list:
				DSHS_sheet['AG'+str(i.row)] = 'Không đạt'
				DSHS_sheet['AH'+str(i.row)] = ''.join(i.why_fail_list)
				print('{} => {}\n'.format(str(i), i.why_fail_list))

		except Exception as err:
			DSHS_sheet['AG'+str(i.row)] = str(err)

	try:
		wb.save('./output.xlsx')
	except Exception as err:
		print(str(err))

if __name__ == '__main__':
	try:
		main()
	except Exception as err:
		print(str(err))
	finally:
		os.system('pause')